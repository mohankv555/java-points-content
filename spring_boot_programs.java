@RestController
public class RestConsumer {
    RestTemplate restTemplate;

    public RestConsumer(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @RequestMapping(value = "/posts")
    public Post[] getProductList() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        return restTemplate.exchange("https://jsonplaceholder.typicode.com/posts", HttpMethod.GET, entity, Post[].class).getBody();
    }

    @RequestMapping(value = "/posts/create")
    public String createPost(@RequestBody Post post) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Post> entity = new HttpEntity<Post>(post, httpHeaders);
        return restTemplate.exchange("https://jsonplaceholder.typicode.com/posts", HttpMethod.POST, entity, String.class).getBody();

    }

    @RequestMapping(value = "/posts/update/{id}")
    public String updatePost(@PathVariable("id") int id, @RequestBody Post post) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Post> entity = new HttpEntity<>(post, httpHeaders);
        return restTemplate.exchange("https://jsonplaceholder.typicode.com/posts/" + id, HttpMethod.PUT, entity, String.class).getBody();
    }

    @RequestMapping(value = "/posts/delete/{id}")
    public String deletePost(@PathVariable("id") int id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        return restTemplate.exchange("https://jsonplaceholder.typicode.com/posts/" + id, HttpMethod.DELETE, entity, String.class).getBody();
    }

    =======================
    private void saveInv() {
        // 1. Producer application URL
        String url = "http://localhost:8080/invoice/rest/saveInvoice";
        // Send JSON data as Body
        String body = "{\"name\":\"INV11\", \"amount\":234.11,\"number\":\"INVOICE11\",\"receivedDate\":\"28-10-2020\",\"type\":\"Normal\",\"vendor\":\"ADHR001\",\"comments\" :\"On Hold\"}";
        // Http Header 
        HttpHeaders headers = new HttpHeaders();
        //Set Content Type
        headers.setContentType(MediaType.APPLICATION_JSON);
        //requestEntity : Body+Header
        HttpEntity<String> request = new HttpEntity<String> (body,headers);
        // 2. make HTTP call and store Response (URL,ResponseType)
    //  ResponseEntity<String> response =  restTemplate.postForEntity(url, request, String.class);
        ResponseEntity<String> response =  restTemplate.exchange(url, HttpMethod.POST,request, String.class);
        // 3. Print details(body,status..etc)
        logger.info("Response Body : {}", response.getBody());
        logger.info("Status code value : {}", response.getStatusCodeValue());
        logger.info("Status code : {}", response.getStatusCode().name());

    }

    ======================

import lombok.NonNull;

public class NonNullExample extends Something {
  private String name;
  
  public NonNullExample(@NonNull Person person) {
    super("Hello");
    this.name = person.getName();
  }
}

==================================