scopes
====================
The final annotation replaced by @SpringBootApplication, @EnableAutoConfiguration enables Spring Boot’s autoconfiguration
@SpringBootApplication is a 3-in-1 annotation.

@EnableAutoConfiguration: enables the auto-configuration feature of Spring Boot.
@ComponentScan: enables @Component scan on the package to discover and register components as beans in Spring’s application Context.
@Configuration: allows to register extra beans in the context or imports additional configuration classes.

-----------------------------------------
If you include the @ComponentScan annotation, then all application components will be registered as Spring Beans automatically. That includes @Service, @Component, @Repository, @Controller, and others.

@Bean - indicates that a method produces a bean to be managed by Spring.
        Spring will handle the life cycle of the beans for you, and it will use these methods to create the beans.
@Service - indicates that an annotated class is a service class.
           The @Service marks a Java class that performs some service, such as execute business logic, perform calculations and call external APIs
@Repository - indicates that an annotated class is a repository, which is an abstraction of data access and storage.
            - annotation is used to define a repository
            - This annotation is used on Java classes which directly access the database. The @Repository annotation works as marker for any class that fulfills the role of repository or Data Access Object.

@Configuration - indicates that a class is a configuration class that may contain bean definitions.

@Controller - marks the class as web controller, capable of handling the requests.
            - Spring will look at the methods of the class marked with the @Controller annotation and establish the routing table to know which methods serve which endpoints.

@RequestMapping - maps HTTP request with a path to a controller method.
@Autowired - marks a constructor, field, or setter method to be autowired by Spring dependency injection.
@SpringBootApplication - enables Spring Boot autoconfiguration and component scanning

@Entity annotation specifies that the class is an entity and is mapped to a database table
@Table annotation specifies the name of the database table to be used for mapping
@Id annotation specifies the primary key of an entity
@GeneratedValue provides for the specification of generation strategies for the values of primary keys.
@Transactional annotation is either complete or rollback
@Enumerated(EnumType.STRING) Enumerated annotation converts the enum into a String
    private Department department;
@NotBlank and @NotNull: These two annotation checks and validate the fields where they are mapped to ensure the values are not blank and null.
@Email: It validates that the annotated field is a valid email address. 
@NotNull: a constrained CharSequence, Collection, Map, or Array is valid as long as it's not null, but it can be empty.
@NotEmpty: a constrained CharSequence, Collection, Map, or Array is valid as long as it's not null, and its size/length is greater than zero.
@NotBlank: a constrained String is valid as long as it's not null, and the trimmed length is greater than zero
@Nullable
-------------------
lombok dependency
@Getter @Setter
@Setter(AccessLevel.PROTECTED) private String name;
  
@AllArgsConstructor - is a Lombok annotation that generates a constructor with all the member variables for the Post class.
@NoArgsConstructor - is a Lombok annotation that generates an empty constructor for the Post class.
@Data - annotation generates getters and setters for the member variables of the Post class.
----------------
Thanks to Lombok and @Data Annotation we get the getter and setter methods for free. Furthermore, @Data generates the following methods automatically:

equals()
hashCode()
toString()
Constructor with all fields that are annotated with @NonNull
@NoArgsConstructor generates a parameterless constructor and @AllArgsConstructor generates a constructor with all parameters.
----------------------------


This annotation has a automatic translation feature. For example, when an exception occurs in the @Repository there is a handler for that exception and there is no need to add a try catch block.

@Profile - adds beans to the application only when that profile is active.


@ResponseBody is a utility annotation that makes Spring bind a method's return value to the HTTP response body. When building a JSON endpoint, this is an amazing way to magically convert your objects into JSON for easier consumption                                                                                           
    The @ResponseBody annotation tells a controller that the object returned is automatically serialized into JSON and passed back into the HttpResponse object.

@RestController is a convenience annotation which combines @Controller and @ResponseBody.
             a convenience syntax for @Controller and @ResponseBody together. This means that all the action methods in the marked class will return the JSON response.                                                                                                       
@RequestMapping(method = RequestMethod.GET, value = "/path") - The @RequestMapping(method = RequestMethod.GET, value = "/path") annotation specifies a method in the controller that should be responsible for serving the HTTP request to the given path. Spring will work the implementation details of how it's done. You simply specify the path value on the annotation and Spring will route the requests into the correct action methods.

@RequestParam(value="name", defaultValue="World") - Naturally, the methods handling the requests might take parameters. To help you with binding the HTTP parameters into the action method arguments, you can use the @RequestParam(value="name", defaultValue="World") annotation. Spring will parse the request parameters and put the appropriate ones into your method arguments.
    This annotation is used to annotate request handler method arguments. Sometimes you get the parameters in the request URL, mostly in GET requests. In that case, along with the @RequestMapping annotation you can use the @RequestParam annotation to retrieve the URL parameter and map it to the method argument. The @RequestParam annotation is used to bind request parameters to a method parameter in your controller.

@PathVariable("placeholderName") - Another common way to provide information to the backend is to encode it in the URL. Then you can use the @PathVariable("placeholderName") annotation to bring the values from the URL to the method arguments.
This annotation is used to annotate request handler method arguments. The @RequestMapping annotation can be used to handle dynamic changes in the URI where certain URI value acts as a parameter. You can specify this parameter using a regular expression. The @PathVariable annotation can be used declare this parameter.



@RequestAttribute
This annotation is used to bind the request attribute to a handler method parameter. Spring retrieves the named attributes value to populate the parameter annotated with @RequestAttribute. While the @RequestParam annotation is used bind the parameter values from query string, the @RequestAttribute is used to access the objects which have been populated on the server side.

@RequestBody
you will get your values mapped with the model you created in your system for handling any specific call.

This annotation is used to annotate request handler method arguments. The @RequestBody annotation indicates that a method parameter should be bound to the value of the HTTP request body. The HttpMessageConveter is responsible for converting from the HTTP request message to object.

----------------------------------

@GetMapping
This annotation is used for mapping HTTP GET requests onto specific handler methods. @GetMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.GET)
@PostMapping
This annotation is used for mapping HTTP POST requests onto specific handler methods. @PostMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.POST)
@PutMapping
This annotation is used for mapping HTTP PUT requests onto specific handler methods. @PutMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.PUT)
@PatchMapping
This annotation is used for mapping HTTP PATCH requests onto specific handler methods. @PatchMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.PATCH)
@DeleteMapping
This annotation is used for mapping HTTP DELETE requests onto specific handler methods. @DeleteMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.DELETE)
[divider style=”4″]

=================================================
@ExceptionHandler
This annotation is used at method levels to handle exception at the controller level. The @ExceptionHandler annotation is used to define the class of exception it will catch. You can use this annotation on methods that should be invoked to handle an exception. The @ExceptionHandler values can be set to an array of Exception types. If an exception is thrown that matches one of the types in the list, then the method annotated with matching @ExceptionHandler will be invoked.

@ControllerAdvice
It allows you to handle exceptions across the whole application, not just to an individual controller.
This annotation is applied at the class level. As explained earlier, for each controller you can use @ExceptionHandler on a method that will be called when a given exception occurs. But this handles only those exception that occur within the controller in which it is defined. To overcome this problem you can now use the @ControllerAdvice annotation. This annotation is used to define @ExceptionHandler, @InitBinder and @ModelAttribute methods that apply to all @RequestMapping methods. Thus if you define the @ExceptionHandler annotation on a method in @ControllerAdvice class, it will be applied to all the controllers.

@ControllerAdvice
public class ProductExceptionController {
   @ExceptionHandler(value = ProductNotfoundException.class)
   public ResponseEntity<Object> exception(ProductNotfoundException exception) {
      return new ResponseEntity<>("Product not found", HttpStatus.NOT_FOUND);
   }
}

@RestControllerAdvice
This annotation is applied on Java classes. @RestControllerAdvice is a convenience annotation which combines @ControllerAdvice and @ResponseBody. This annotation is used along with the @ExceptionHandler annotation to handle exceptions that occur within the controller.

=====================================


@Async
This annotation is used on methods to execute each method in a separate thread

@Lazy
This annotation is used on component classes. By default all autowired dependencies are created and configured at startup. But if you want to initialize a bean lazily, you can use @Lazy annotation over the class. This means that the bean will be created and initialized only when it is first requested for. You can also use this annotation on @Configuration classes. This indicates that all @Bean methods within that @Configuration should be lazily initialized.




==============================================
@Repository
public interface CityRepository extends CrudRepository<City, Long> {

}
====================


@Controller
@RequestMapping("users")
public class UserController {

    @GetMapping("/{id}", produces = "application/json")
    public @ResponseBody User getUser(@PathVariable int id) {
    	return findUsersById(id);
    }

    private User findUsersById(int id) {
    		// return user specific data
    }
}
================
@RequestParam
As seen in the code above, @RequestParam allows you to send parameters in the get request and use them in Java. 
It also supplies a default value.

===================

@Validated
Java
To validate input for methods, you apply the @Validated annotation to the class

import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
public class TestBean {
public Archive findByCopiesAndTitle(@Size(min = 1, max = 100) String code, @NotNull Title title) {
return …
}
}
========================
@Value annotation to inject values from a property file into a bean’s attribute

For example, let’s first define a property file, then inject values of properties using @Value.

server.port=9898
server.ip= 10.10.10.9
emp.department= HR
columnNames=EmpName,EmpSal,EmpId,EmpDept
Now inject the value of server.ip using @Value as below:

@Value("${server.ip}")
private String serverIP;

@Value for multiple values

@Value("${columnNames}")
private String[] columnNames;

==================================
@Autowired lazy beans
Generally, beans are injected into other components using @Autowired annotation. In this case, we must use the lazy annotation at both places:

The bean definition which you want to lazy load
The place it is injected along with @Autowired annotation
@Lazy
@Service
public class EmployeeManagerImpl implements EmployeeManager {
  //
}
@Controller
public class EmployeeController {
 
    @Lazy
    @Autowired
    EmployeeManager employeeManager;
}
===============================
@SpringBootTest—that lets you test using Spring Boot specific features.

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.mock.mockito.*;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.*;

@SpringBootTest
class ServiceTests {

    @MockBean
    private RemoteService remoteService;

    @Autowired
    private Capitalizer capitalizer;

    @Test
    void exampleTest() {
        // RemoteService has been injected into the capitalizer bean
        given(this.remoteService.testCall()).willReturn("test");
        String caps = capitalizer.capitalizeTestCall();
        assertThat(caps).isEqualTo("TEST");
    }

}
==================================
spring Boot Actuator is a sub-project of the Spring Boot Framework. It includes a number of additional features that help us to monitor and manage the Spring Boot application. It contains the actuator endpoints (the place where the resources live). We can use HTTP and JMX endpoints to manage and monitor the Spring Boot application. If we want to get production-ready features in an application, we should use the Spring Boot actuator.

Spring Boot Actuator Features
There are three main features of Spring Boot Actuator:

Endpoints
Metrics
Audit

in application.properties

management.security.enabled=false  
Step 10: Run the SpringBootActuatorExampleApplication.java file.

Step 11: Open the browser and invoke the URL http://localhost:8080/actuator. It returns the following page:

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>

=====================================================================================
Spring Boot - Building RESTful Web Services

application.properties
 spring.datasource.url=jdbc:mysql://localhost:3306/employee_db?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false
 spring.datasource.username=root
 spring.datasource.password=mypassword
 server.port=8081



<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-web</artifactId>    
</dependency>
---------------------------------------------------------
 request URI is /products 

@RestController
public class ProductServiceController {
   private static Map<String, Product> productRepo = new HashMap<>();
   static {
      Product honey = new Product();
      honey.setId("1");
      honey.setName("Honey");
      productRepo.put(honey.getId(), honey);
      
      Product almond = new Product();
      almond.setId("2");
      almond.setName("Almond");
      productRepo.put(almond.getId(), almond);
   }
    @RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
   public ResponseEntity<Object> delete(@PathVariable("id") String id) { 
      productRepo.remove(id);
      return new ResponseEntity<>("Product is deleted successsfully", HttpStatus.OK);
   }
   
   @RequestMapping(value = "/products/{id}", method = RequestMethod.PUT)
   public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Product product) { 
      productRepo.remove(id);
      product.setId(id);
      productRepo.put(id, product);
      return new ResponseEntity<>("Product is updated successsfully", HttpStatus.OK);
   }
   
   @RequestMapping(value = "/products", method = RequestMethod.POST)
   public ResponseEntity<Object> createProduct(@RequestBody Product product) {
      productRepo.put(product.getId(), product);
      return new ResponseEntity<>("Product is created successfully", HttpStatus.CREATED);
   }
   
   @RequestMapping(value = "/products")
   public ResponseEntity<Object> getProduct() {
      return new ResponseEntity<>(productRepo.values(), HttpStatus.OK);
   }


}
============================
Rest Template examples

 restTemplate.exchange("http://localhost:8080/products", HttpMethod.GET, entity, String.class).getBody();

 ResponseEntity<Person[]> response = restTemplate.getForEntity(ROOT_URI, Person[].class);
 ResponseEntity<Person[]> response = restTemplate.getForEntity(ROOT_URI, Person[].class, params);
        
--------------

 public List<Employee> getAll(int page, int pageSize) {
  String requestUri = REQUEST_URI + "?page={page}&pageSize={pageSize}";
  Map<String, String> urlParameters = new HashMap<>();
  urlParameters.put("page", Integer.toString(page));
  urlParameters.put("pageSize", Long.toString(pageSize));
  ResponseEntity<Employee[]> entity = restTemplate.getForEntity(requestUri,
                                                                Employee[].class,
                                                                urlParameters);
  return entity.getBody() != null? Arrays.asList(entity.getBody()) :                          
                                   Collections.emptyList();
}

-----------
public Optional<Employee> getForObject(long id) {
  Employee employee = restTemplate.getForObject(REQUEST_URI + "/{id}",
                                                Employee.class,
                                                Long.toString(id));
  return Optional.ofNullable(employee);
}
----------------

public JsonNode getAsJsonNode(long id) throws IOException {
  String jsonString = restTemplate.getForObject(REQUEST_URI + "/{id}",
                                                String.class,
                                                id);
  ObjectMapper mapper = new ObjectMapper();
  return mapper.readTree(jsonString);
}

--------------------
RestTemplate rest = new RestTemplate();
String url = "https://jsonplaceholder.typicode.com/posts?userId={uid}";
// initialize params map
Map<String, String> params = new HashMap<>();
// add entry for query parameter
params.put("uid", "1");
// fetch response as a post object
Post[] response = rest.getForObject(url, Post[].class, params);
System.out.println(response[0].toString());

ResponseEntity<String> entity = rest.getForEntity(url, String.class, params);

----------------



-------------------------------------------------------
import lombok to use setter, getter, 
@Setter
@Getter
public class EmployeeRequest {
    @NotBlank
    @NotNull
    private String firstName;
    @NotBlank
    @NotNull
    private String lastname;
    @NotBlank
    @NotNull
    private String phoneNumber;
    @Email
    private String email;
    @NotBlank
    @NotNull
    private double salary;
    @NotBlank
    @NotNull
    @Enumerated(EnumType.STRING)
    private Department department;

    
}

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
====================
